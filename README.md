# omeka-export

This tool performs various data exports from the Omeka instance.

Its install is done at runtime from the instance
[`user_data`](https://gitlab.com/bibliosansfrontieres/tm/tmcloud/terraform/-/commit/6d7f76213bd35bed67ce8cd7a96e75130bc0564a).

## Scripts and exports

* [database exports](doc/db-exports.md)
* [projects](doc/projects.md)
* [projects](doc/items.md)
* [missing files](doc/missing-files.md)
* [exports archival](doc/rsync-to-s3.md)

## Adding new scripts

Drop your export scripts into the `scripts.d/` directory, following these rules:

* the file must be executable
* the file must be readable by the `www-data` user
* the filename must consist of alphanumerics, underscores and dashes (no dot)

See `man 8 run-parts` for details.

## FAQ

### Why is the main (bash) script named `.py`?

As explained in [#14](https://gitlab.com/bibliosansfrontieres/omeka/omeka-export/-/issues/14),
the terraform plan used to build the Omeka instance is quite stuck,
so is the file name.

The good thing is, Linux does not care about the file name
but will happily respect its [shebang](https://en.wikipedia.org/wiki/Shebang_(Unix)).

## TODO

* [x] export files to a specific path (preferably in the `DocumentRoot`)
  * [ ] allow override from environment vars
* [ ] error handling
