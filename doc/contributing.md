# Development

## Get a database dump from production

```shell
./tools/get_db_dump_from_prod.sh
```

The production database is dumped to `tools/prod.sql`.

## Run a local MySQL server with the DB dump loaded

```shell
./tools/run_local_mysqld.sh
```

## Run queries

```shell
./tools/mysql_local.sh < some_query_file.sql
```

## Get a MySQL shell

```shell
./tools/mysql_local.sh
```
