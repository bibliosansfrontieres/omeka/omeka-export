# db-exports

The `scripts.d/db-exports` runs various exports from Omeka
[to CSV files](https://omeka.tm.bsf-intranet.org/exports/).

It is intended to query RDS right from the Omeka instance.

## Requirements

The script uses the [`mysqlclient` python module](https://pypi.org/project/mysqlclient/),
installed via the [`python3-mysqldb` package](https://packages.debian.org/search?keywords=python3%2Dmysqldb).

It also gets the SQL credendials from `~admin/.my.cnf`.
This file is automatically built at the
[cloud-init stage](https://gitlab.com/bibliosansfrontieres/tm/tmcloud/terraform/-/commit/4cd076a9a30f5e6a1064c2e7a52baed84d99ab54).

## Usage

```shell
./scripts.d/db-exports
```

The export files are stored in `/var/www/omeka.bsf-intranet.org/exports/`.

## Queries

### itemsbypackage

| item_id | package_id |
| ------- | ---------- |
|9216 | 1588 199 96 |

### orpheans

Items which do NOT belong to any package.

|item_id|size|mime_type|filename|added|modified|
| ----- | -- | ------- | ------ | --- | ------ |
|9262|22437529|audio/mpeg|saving_the_media_converted.mp3|1999-12-31 23:00:00|2019-03-15 16:43:05|

### all items

Items - all the items!

|item_id|size|mime_type|filename|added|modified|
| ----- | -- | ------- | ------ | --- | ------ |
|9262|22437529|audio/mpeg|saving_the_media_converted.mp3|1999-12-31 23:00:00|2019-03-15 16:43:05|

### items filenames

Items and their filenames.

This query is a simplified version of the `allitems` query,
and is intented to be used for checking for missing files.

|item_id|filename|
| ----- | ------ |
|9262|saving_the_media_converted.mp3|

### packages

|id|name|ideascube_name|description|global_ojective|audience|language|subject|education_level|added|modified|relations|
|--|----|--------------|-----------|---------------|--------|--------|-------|---------------|-----|--------|---------|
|6|"Découvrez différents sports !! Paquet redécoupé. Voir les paquets #856 857 858 313"|Découvrez différents sports|Vous souhaitez vous perfectionner au football ? Vous initier au yoga ou faire un exercice physique quotidien ? C'est ici !|Permettre aux personnes d'accéder à des loisirs et de se relaxer.|adultes|fr|||2017-03-27 15:25:34|2022-03-25 11:14:51|16954 18191 18667 18847 19291 19972 20805 21664 23838 23840 23842 24337 24345 31228 31231 31455 33825 35792 35793 36027 39001 39052 39053 39054 39055 40107 40108 40110 47336 63967|
