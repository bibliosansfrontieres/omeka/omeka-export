# Items

Export of items.

Sample:

|Item ID|Title|Description|Creator|Publisher|Language|Audience|License|Duration|Source|Subject|Format|Collection|Tags|Filename|Package ID|Added|Modified|
|-------|-----|-----------|-------|---------|--------|--------|-------|--------|------|-------|------|----------|----|--------|----------|-----|--------|
|11608|Épisode 06 : Elle est policière|"En 25 épisodes de sept minutes, Le Talisman brisé est une histoire qui offre aux auditeurs et apprenants un jeu de pistes, une expérience d’immersion dans la peau du héros où l’apprentissage de la langue est la condition de réussite de l’enquête de Kwamé."|RFI|Oxford University|en|"adolescents, teenagers, المراهقين, enfants, الاطفال, adultes, adults, الكبار"|CC-BY-SA|00:11:22|https://youtu.be/example|FLE|mp3|32|"français, french, languages, langues, اللغةالفرنسية, لغات"|cours_talisman_brise_episode_6_256.mp3|208 1693 14|2017-03-13T12:31:21+00:00|2021-03-22T15:47:06+00:00|

## Technical considerations

Similarily to the [projects](projects.md) export,
the database relations are much complicated ; besides, we also
want to get the packages relations, and the filename.

We loop over the Items `item_id` found from a previous DB export,
querying the API for the metadata and also for the filename.
