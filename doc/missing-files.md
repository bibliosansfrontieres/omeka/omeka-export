# missing-files export

It turns out some files seem to disappear from the filesystem
(cf [#11](https://gitlab.com/bibliosansfrontieres/omeka/omeka-export/-/issues/11)).

This export is a list of files that are referenced in the database
but are missing from the filesystem.

Basically, the script loops over files items,
and checks whether the file exists on the filesystem.
If the file is missing, it is added along its ID to the list.

## Sample

|item_id|full_path|
|-------|---------|
|10800|/var/www/omeka.bsf-intranet.org/files/original/decentralisation_fiscale_burundi.mp4|
|11955|/var/www/omeka.bsf-intranet.org/files/original/nassam_alina.mp3|
