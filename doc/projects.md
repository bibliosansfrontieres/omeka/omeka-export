# projects

Export of `BSF projects` type items.

Sample:

|Project ID|Title|Description|Date de début de déploiement|Partenaires|Lieu de déploiement|Outil|Chargé(e) de projet BSF|Creator|Added|Modified|
|----------|-----|-----------|----------------------------|-----------|-------------------|-----|-----------------------|-------|-----|--------|
|16954|"KB_SAMU SOCIAL_FRANCE_PARIS"|Koombook de démo en prévision de la sélection des 4 autres kits koombooks.|Avril 2017|Samu Social de Paris|4 centres d'hébergement en région parisienne.|Kits Koombooks|Léa Maris|-|2017-04-04T11:08:02+00:00|2024-03-20T10:46:05+00:00|

## Technical considerations

Retrieving the fields and their labels and their values directly
from the database is a headache. The workaround strategy is:

- first, export the items[type=19] from the DB to a CSV file
- then, use the API to get each item's metadata set
- write it all to a CSV file and delete the former one
