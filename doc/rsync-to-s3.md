# rsync-to-s3

This script keeps the generated export files to the S3 bucket.

This allows for diff between consequent runs.

## Future plans

- maybe only save files that differ from the previous run
  (saves a few $$, complicates things when it comes to run diffs)
