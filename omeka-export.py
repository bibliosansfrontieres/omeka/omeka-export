#!/bin/bash
# -- yes this is confusing, cf #14
#
# vim: ft=shell :

# follow the symlink
installdir="$( cd "$( dirname "$( readlink "${BASH_SOURCE[0]}" )" )" && pwd )"
SCRIPTSD="${installdir}/scripts.d"

# run 'em all
chmod -R +x "$SCRIPTSD"
run-parts -v "$SCRIPTSD"
