#!/usr/bin/env python3

import csv
import requests
import boto
import json
import glob
import os
import sys
from datetime import datetime
import logging

logging.basicConfig(
    format="%(asctime)s - %(levelname)-8s - %(message)s", level=logging.INFO
)


def get_latest_csv_file(directory):
    """Return the most recent file based on modification time"""
    csv_files = glob.glob(os.path.join(directory, "export-temporary-projects-*.csv"))

    if not csv_files:
        logging.error("No CSV files found in the exports directory.")
        sys.exit(1)

    return max(csv_files, key=os.path.getmtime)


exports_directory = "/var/www/omeka.bsf-intranet.org/exports/"
projects_items_id_list = get_latest_csv_file(exports_directory)


# load projects IDs from the CSV file
with open(
    projects_items_id_list, mode="r", newline="", encoding="utf-8"
) as projects_list:
    reader = csv.reader(projects_list)
    project_ids = [row[0] for row in reader]

# prepare CSV output file
timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
output_file_path = f"{exports_directory}export-projectsmetadata-{timestamp}.csv"

logging.info(f"Exporting projects to: {output_file_path}")

with open(output_file_path, mode="w", newline="", encoding="utf-8") as csvfile:
    writer = csv.writer(csvfile, lineterminator="\n")
    writer.writerow(
        [
            "Project ID",
            "Title",
            "Description",
            "Date de début de déploiement",
            "Partenaires",
            "Lieu de déploiement",
            "Outil",
            "Chargé(e) de projet BSF",
            "Creator",
            "Added",
            "Modified",
        ]
    )

    # get the Omeka API key from secrets bucket
    conn = boto.connect_s3()
    bucket_name = "omeka-secrets-prod-eu-west-1-481031604408"
    file_key = "script_user.apiKey"
    bucket = conn.get_bucket(bucket_name)
    key = bucket.get_key(file_key)
    api_key = key.get_contents_as_string().decode("utf-8")

    # fetch metadata for each project and write to CSV
    for project_id in project_ids:
        logging.debug(f"[+] Fetching project id {project_id}")
        response = requests.get(
            f"http://omeka.tm.bsf-intranet.org/api/items/{project_id}?key={api_key}"
        )
        if response.status_code == 200:
            project_data = response.json()
            element_texts = {
                item["element"]["name"]: item["text"]
                for item in project_data["element_texts"]
            }
            row = [
                project_id,
                element_texts.get("Title", ""),
                element_texts.get("Description", ""),
                element_texts.get("Date de début de déploiement", ""),
                element_texts.get("Partenaires", ""),
                element_texts.get("Lieu de déploiement", ""),
                element_texts.get("Outil", ""),
                element_texts.get("Chargé(e) de projet BSF", ""),
                element_texts.get("Creator", ""),
                project_data.get("added", ""),
                project_data.get("modified", ""),
            ]
            writer.writerow(row)

    # remove the temporary CSV file
    try:
        logging.info(f"Remove temporary CSV file")
        os.remove(projects_items_id_list)
    except OSError as e:
        logging.error(f"Error: failed to remove {projects_items_id_list}")
