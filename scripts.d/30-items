#!/usr/bin/env python3

import csv
import requests
import boto
import json
import glob
import os
import sys
from datetime import datetime
import logging

logging.basicConfig(
    format="%(asctime)s - %(levelname)-8s - %(message)s", level=logging.INFO
)


def get_latest_csv_file(directory):
    """Return the most recent file based on modification time"""
    csv_files = glob.glob(os.path.join(directory, "export-itemsbypackage-*.csv"))

    if not csv_files:
        logging.error("No CSV files found in the exports directory.")
        sys.exit(1)

    return max(csv_files, key=os.path.getmtime)


exports_directory = "/var/www/omeka.bsf-intranet.org/exports/"
items_id_list = get_latest_csv_file(exports_directory)


# load items IDs and package IDs from the CSV file
package_dict = {}
with open(items_id_list, mode="r", newline="", encoding="utf-8") as items_list:
    reader = csv.reader(items_list)
    for row in reader:
        item_id, package_id = row
        package_dict[item_id] = package_id
    item_ids = list(package_dict.keys())

# prepare CSV output file
timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
output_file_path = f"{exports_directory}export-itemsmetadata-{timestamp}.csv"

logging.info(f"Exporting items to: {output_file_path}")

with open(output_file_path, mode="w", newline="", encoding="utf-8") as csvfile:
    writer = csv.writer(csvfile, lineterminator="\n")
    writer.writerow(
        [
            "Item ID",
            "Title",
            "Description",
            "Creator",
            "Publisher",
            "Language",
            "Audience",
            "License",
            "Duration",
            "Source",
            "Subject",
            "Format",
            "Collection",
            "Tags",
            "Filename",
            "Package ID",
            "Added",
            "Modified",
        ]
    )

    # get the Omeka API key from secrets bucket
    conn = boto.connect_s3()
    bucket_name = "omeka-secrets-prod-eu-west-1-481031604408"
    file_key = "script_user.apiKey"
    bucket = conn.get_bucket(bucket_name)
    key = bucket.get_key(file_key)
    api_key = key.get_contents_as_string().decode("utf-8")

    # fetch metadata for each item and write to CSV
    for item_id in item_ids:
        logging.debug(f"[+] Fetching item id {item_id}")
        response = requests.get(
            f"http://omeka.tm.bsf-intranet.org/api/items/{item_id}?key={api_key}"
        )
        if response.status_code == 200:
            item_data = response.json()
            element_texts = {
                item["element"]["name"]: item["text"]
                for item in item_data["element_texts"]
            }
            tags = ", ".join(tag["name"] for tag in item_data.get("tags", []))
            audience = ", ".join(
                item["text"]
                for item in item_data["element_texts"]
                if item["element"]["name"] == "Audience"
            )
            # Fetch file details to get the original filename
            file_response = requests.get(item_data["files"]["url"])
            filename = ""
            if file_response.status_code == 200:
                files_data = file_response.json()
                if files_data:
                    filename = files_data[0].get("original_filename", "")

            row = [
                item_id,
                element_texts.get("Title", ""),
                element_texts.get("Description", ""),
                element_texts.get("Creator", ""),
                element_texts.get("Publisher", ""),
                element_texts.get("Language", ""),
                audience,
                element_texts.get("License", ""),
                element_texts.get("Duration", ""),
                element_texts.get("Source", ""),
                element_texts.get("Subject", ""),
                element_texts.get("Format", ""),
                item_data["collection"]["id"] if item_data.get("collection") else "",
                tags,
                filename,
                package_dict.get(item_id, ""),
                item_data.get("added", ""),
                item_data.get("modified", ""),
            ]
            writer.writerow(row)
