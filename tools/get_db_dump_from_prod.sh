#!/bin/bash

gitroot=$( git rev-parse --show-toplevel )

ssh omeka.prod.tm.aws \
    'mysqldump omeka | gzip -9' \
    | gunzip - \
    > "${gitroot}/tools/prod.sql"
