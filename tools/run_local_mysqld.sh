#!/bin/bash


#2024-02-15 11:12:15 0 [Warning] InnoDB: Total InnoDB FTS size 9911266 for the table `omeka`.`omeka_search_texts` exceeds the innodb_ft_cache_size 8000000
#2024-02-15 11:12:18 0 [Warning] InnoDB: Total InnoDB FTS size 8667764 for the table `omeka`.`omeka_search_texts` exceeds the innodb_ft_cache_size 8000000
#2024-02-15 11:12:20 0 [Warning] InnoDB: Total InnoDB FTS size 9017235 for the table `omeka`.`omeka_search_texts` exceeds the innodb_ft_cache_size 8000000


img='mysql:5.7'
img='mariadb:latest'

gitroot="$( git rev-parse --show-toplevel )"
DBFILE="${gitroot}/tools/prod.sql"

chmod a+r "$DBFILE"
docker run --rm --name omekadb \
    -v "${DBFILE}:/docker-entrypoint-initdb.d/prod.sql" \
    --env MARIADB_ALLOW_EMPTY_ROOT_PASSWORD=1 \
    --env MARIADB_DATABASE=omeka \
    --env MARIADB_USER=omeka \
    --env MARIADB_PASSWORD=omeka \
    -p 3306:3306 \
    "$img"

chmod a-r "$DBFILE"
